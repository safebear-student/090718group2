Feature: Task Management

  User Story:

  In order to manage my tasks
  As a user
  I need to be able to add, delete, update tasks

  Rules / Acceptance criteria:

   - You must be able to view your tasks
   - They must be paginated

  Questions:
   - How many tasks to display on screen at once?

  To do:
   - Pagination

  Domain language:

  Task = A task is made up of a name, a due date and a status
  Status = not completed / competed

  # 12. One of the steps is highlighted below because the step definition is missing. Can you fix this?

  @high-risk
  @high-impact
  Scenario Outline: A user creates a task
    When a user creates a <task>
    Then the <task> appears in the tasklist
    Examples:
      | task |
      |buff a hippo|
